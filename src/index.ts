import * as PIXI from "pixi.js";

let app = new PIXI.Application({backgroundColor: 0x22AA00});
document.body.appendChild(app.view);

let bunny = PIXI.Sprite.from("assets/bunny.jpg");
bunny.anchor.set(0.5);
bunny.x = app.screen.width / 2;
bunny.y = app.screen.height / 2;

app.stage.addChild(bunny);

app.ticker.add(function(delta) {
    bunny.rotation += 0.001 * delta;
});
